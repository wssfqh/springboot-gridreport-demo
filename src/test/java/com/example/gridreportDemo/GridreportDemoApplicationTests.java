package com.example.gridreportDemo;

import com.example.gridreportDemo.data.DataTextProvider;
import gridreport.jni.*;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.ResourceLoader;

import java.io.IOException;

@SpringBootTest
class GridreportDemoApplicationTests {

    @Autowired
    ResourceLoader resourceLoader;

    @Test
    void contextLoads() throws IOException {
        Report report = null;
        try {
            report = new Report();
            String reportAbsolutePath = resourceLoader.getResource("classpath:report/1o.文字绕行与自动伸展.grf").getFile().getAbsolutePath();
            report.LoadFromFile(reportAbsolutePath);
            String dataAbsolutePath = resourceLoader.getResource("classpath:data/json/jsonCategories.txt").getFile().getAbsolutePath();
            report.LoadDataFromURL(dataAbsolutePath);
            BinaryObject bo = report.ExportDirectToBinaryObject(ExportType.PDF);
//            bo.SaveToFile("gr-jni-LoadDataFromURL.pdf");
//            System.out.println(bo.getDataBuf());


        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.getMessage());
        }

    }

    @Test
    public void test2() {
        Report report = null;
        try {

            report = new Report();
            String reportAbsolutePath = resourceLoader.getResource("classpath:report/1o.文字绕行与自动伸展.grf").getFile().getAbsolutePath();
            report.LoadFromFile(reportAbsolutePath);
            String dataAbsolutePath = resourceLoader.getResource("classpath:data/json/jsonCategories.txt").getFile().getAbsolutePath();
            report.LoadDataFromURL(dataAbsolutePath);
            //设置打印相关的参数
            gridreport.jni.Printer printer = report.getPrinter();
            printer.setPrinterName("Adobe PDF");
            report.Print(false);
            System.out.println("Print " + reportAbsolutePath + "on printer name as " + printer.getPrinterName());
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.getMessage());
        }
    }

}
