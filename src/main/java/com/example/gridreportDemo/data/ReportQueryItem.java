package com.example.gridreportDemo.data;

public class ReportQueryItem {
	String QuerySQL;
	String RecordsetName;

	public ReportQueryItem(String AQuerySQL, String ARecordsetName)
	{
		QuerySQL = AQuerySQL;
		RecordsetName = ARecordsetName;
	}
}
