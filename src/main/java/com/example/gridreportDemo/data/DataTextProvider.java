package com.example.gridreportDemo.data;

import java.util.ArrayList;

public class DataTextProvider {
	public static String Build(String QuerySQL) throws Exception {
		return ReportJsonData.OneRecordset(QuerySQL);
	}

	public static String BuildList(ArrayList<ReportQueryItem> QueryItems) throws Exception {
		return ReportJsonData.MultiRecordset(QueryItems);
	}

	public static String AppendBlankRow() throws Exception {
		String QuerySQL = "select m.OrderId, m.OrderDate, d.Productid,p.ProductName,d.Quantity,"
			+ "d.UnitPrice*d.Quantity as Amount "
			+ "from orders m inner join (orderdetails d inner join products p on d.ProductID=p.ProductID) "
			+ "on m.orderid=d.orderid "
			+ "where (m.OrderDate between '1996-1-1' And '1997-9-30') and d.Productid<10 "
			+ "order by d.ProductID";
		return Build(QuerySQL);
	}

	public static String Categories() throws Exception {
		String QuerySQL = "select * from categories ";
		return Build(QuerySQL);
	}

	public static String ContractOne() throws Exception {
		String QuerySQL = "select m.OrderID,m.CustomerId,c.CompanyName,m.OrderDate, "
			+ "p.ProductName,d.UnitPrice,d.Quantity,d.UnitPrice*d.Quantity as Amount "
			+ "from (orders m inner join "
			+ "(orderdetails as d inner join products p on P.ProductID=D.ProductID) on m.OrderId=d.OrderId) "
			+ "left join customers c on c.CustomerID=m.CustomerID "
			+ "where m.OrderID=10252 and d.ProductID=20 "
			+ "order by m.OrderDate, m.OrderID";
		return Build(QuerySQL);
	}

	public static String CrossTab() throws Exception {
		String QuerySQL =  "select c.City,m.CustomerId,c.CompanyName,d.ProductID,p.ProductName,"
			+ "d.Quantity, d.UnitPrice*d.Quantity as Amount "
			+ "from (orders m inner join "
			+ "(orderdetails as d inner join products p "
			+ "on P.ProductID=D.ProductID) on m.OrderId=d.OrderId) "
			+ "left join customers c on c.CustomerID=m.CustomerID "
			+ "where d.ProductID<8 "
			+ "order by c.City,m.CustomerId, d.ProductID";
		return Build(QuerySQL);
	}
		
	public static String CrossTabByDay() throws Exception {
		String QuerySQL =  "select c.CompanyName,m.OrderDate,d.UnitPrice*d.Quantity as Amount "
		+ "from (orders m inner join orderdetails as d on m.OrderId=d.OrderId) "
		+ "left join customers c on c.CustomerID=m.CustomerID "
		+ "where m.OrderDate between '1997-6-1' and '1997-6-30' "
		+ "order by c.CompanyName, m.OrderDate";
		return Build(QuerySQL);
	}
		
	public static String CrossTabByMonth() throws Exception {
		String QuerySQL =  "select c.CompanyName,m.OrderDate,d.UnitPrice*d.Quantity as Amount "
		+ "from (orders m inner join orderdetails as d on m.OrderId=d.OrderId) "
		+ "left join customers c on c.CustomerID=m.CustomerID "
		+ "where m.OrderDate between '1997-1-1' and '1997-12-31' "
		+ "order by c.CompanyName, m.OrderDate";
		return Build(QuerySQL);
	}

	public static String CrossTabCalendar() throws Exception {
		String QuerySQL =  "select m.OrderDate,sum(d.Quantity) as Qty,sum(d.UnitPrice*d.Quantity) as Amount " +
			"from (orders m inner join orderdetails as d on m.OrderId=d.OrderId) " +
			"where m.OrderDate between '1997-1-1' and '1997-12-31' " +
			"group by m.OrderDate " +
			"order by m.OrderDate";
		return Build(QuerySQL);
	}
		
	public static String CrossTabSubtotal() throws Exception {
		String QuerySQL =  "select t.CategoryName, p.ProductName,c.City,c.CompanyName,d.Quantity " +
			"from (orders m inner join  " +
			"(orderdetails as d inner join (products p inner join categories t on p.CategoryID=t.CategoryID) " +
			"on P.ProductID=D.ProductID) on m.OrderId=d.OrderId) " +
			"left join customers c on c.CustomerID=m.CustomerID " +
			"where m.OrderDate between '1997-1-1' and '1997-3-31' " +
			"order by t.CategoryName,p.ProductName,c.City,c.CompanyName ";
		return Build(QuerySQL);
	}
		
	public static String CrossTabYearMonth() throws Exception {
		String QuerySQL =  
		"select Year(m.OrderDate) As TheYear,Month(m.OrderDate) As TheMonth, sum(d.UnitPrice*d.Quantity) as Amount " +
		"from orders m inner join orderdetails as d on m.OrderId=d.OrderId " +
		"group by Year(m.OrderDate),Month(m.OrderDate) " +
		"order by Year(m.OrderDate),Month(m.OrderDate) ";    
		return Build(QuerySQL);
	}

	public static String Customer() throws Exception {
		String QuerySQL =  "select * from customers";
		return Build(QuerySQL);
	}

	public static String QRCodeMESTM() throws Exception {
		String QuerySQL =  "select * from MESBA";
		return Build(QuerySQL);
	}

	public static String EmployeeOne() throws Exception {
		String QuerySQL =  "select * from employees where EmployeeID=5";
		return Build(QuerySQL);
	}

	public static String InvoiceMany() throws Exception {
		String QuerySQL =  "select m.OrderID,m.CustomerId,c.CompanyName,m.OrderDate,M.Freight,"
		+ "d.ProductID,p.ProductName,d.UnitPrice,d.Quantity,d.UnitPrice*d.Quantity as Amount "
		+ "from (orders m inner join "
		+ "(orderdetails as d inner join products p on P.ProductID=D.ProductID) on m.OrderId=d.OrderId) "
		+ "left join customers c on c.CustomerID=m.CustomerID "
		+ "where m.OrderID>=10255 and m.OrderID<10260";
		return Build(QuerySQL);
	}

	public static String Picture() throws Exception {
		String QuerySQL =  "select EmployeeID,LastName,FirstName,Title,TitleOfCourtesy,BirthDate,HireDate,"
		+ "Address,City,region,PostalCode,Country,HomePhone,Extension,Photo,Notes from employees";
		return Build(QuerySQL);
	}

	public static String RTFSample() throws Exception {
		String QuerySQL =  "select m.OrderID,m.CustomerId,c.CompanyName,c.ContactName,c.Address,c.city,c.region,c.Country,c.Postalcode,"
		+ "m.OrderDate,M.Freight,d.ProductID,p.ProductName,"
		+ "d.UnitPrice,d.Quantity,d.Discount,"
		+ "d.UnitPrice*d.Quantity as Amount,"
		+ "d.UnitPrice*d.Quantity*d.Discount as DiscountAmt,"
		+ "d.UnitPrice*d.Quantity-d.UnitPrice*d.Quantity*d.Discount as NetAmount "
		+ "from (orders m inner join "
		+ "(orderdetails as d inner join products p on P.ProductID=D.ProductID) on m.OrderId=d.OrderId) "
		+ "left join customers c on c.CustomerID=m.CustomerID "
		+ "where m.OrderDate between '1997-1-1' And '1997-1-15' " 
		+ "order by m.CustomerID,m.OrderDate, m.OrderID";
		return Build(QuerySQL);
	}

	public static String SaleByProduct() throws Exception {
		String QuerySQL =  "select m.OrderID,m.OrderDate, " +
			"d.ProductID,p.ProductName,d.UnitPrice,d.Quantity,d.UnitPrice*d.Quantity as Amount  " +
			"from orders m inner join  " +
			"(orderdetails as d inner join products p on P.ProductID=D.ProductID) on m.OrderId=d.OrderId " +
			"where m.OrderDate between '1997-6-1' and '1997-12-31' " +
			"order by d.ProductID, m.OrderDate";
		return Build(QuerySQL);
	}

	public static String SaleDetail() throws Exception {
		String QuerySQL =  "select m.OrderID,m.CustomerId,c.CompanyName,m.OrderDate,M.Freight,"
		+ "d.ProductID,p.ProductName,d.UnitPrice,d.Quantity,d.Discount,"
		+ "d.UnitPrice*d.Quantity as Amount, d.UnitPrice*d.Quantity*d.Discount as DiscountAmt,"
		+ "d.UnitPrice*d.Quantity-d.UnitPrice*d.Quantity*d.Discount as NetAmount "
		+ "from (orders m inner join "
		+ "(orderdetails as d inner join products p on P.ProductID=D.ProductID) on m.OrderId=d.OrderId) "
		+ "left join customers c on c.CustomerID=m.CustomerID "
		+ "where m.OrderID<=10300 "
		+ "order by m.OrderDate, m.OrderID";
		return Build(QuerySQL);
	}

	public static String SaleSumByProduct() throws Exception {
		String QuerySQL =  "select d.Productid,p.ProductName,sum(d.Quantity) as Quantity, " +
			"sum(d.UnitPrice*d.Quantity*(1-d.Discount)) as Amount " +
			"from orders m inner join (orderdetails d inner join products p " +
			"on d.ProductID=p.ProductID) " +
			"on m.orderid=d.orderid " +
			"where m.OrderDate between '1997-1-1' and '1997-12-31' " +
			"group by d.Productid,p.ProductName " +
			"order by d.Productid";
		return Build(QuerySQL);
	}

//	public static String Report_7_3g(HttpServletRequest request) throws Exception {
//		String QuerySQL =  "select * from products " +
//			"where ProductID>=" + request.getParameter("BeginNo") + " and ProductID<=" + request.getParameter("EndNo") +
//			" order by ProductID";
//		return Build(QuerySQL);
//	}

//	public static String FilterSaleSummary(HttpServletRequest request) throws Exception {
//		String QuerySQL =  
//			"select p.CategoryID,c.CategoryName,d.Productid,p.ProductName,sum(d.Quantity) as Quantity," +
//			"sum(d.UnitPrice*d.Quantity) as Amount " +
//			"from orders m inner join (orderdetails d inner join (products p inner join categories c on c.CategoryID=p.CategoryID) " +
//			"on d.ProductID=p.ProductID) on m.orderid=d.orderid " +
//			"where m.OrderDate between '" + request.getParameter("BeginDate") + "' And '" + request.getParameter("EndDate") + "' " +
//			"group by p.CategoryID,c.CategoryName,d.Productid,p.ProductName " +
//			"order by p.CategoryID,d.Productid";
//		return Build(QuerySQL);
//	}
//
//	public static String FilterCustomerProductCrosstab(HttpServletRequest request) throws Exception {
//		String QuerySQL =  
//			"select p.CategoryID,t.CategoryName,p.ProductID,p.ProductName,c.City,c.CompanyName,d.Quantity " +
//			"from (orders m inner join  " +
//			"(orderdetails as d inner join (products p inner join categories t on p.CategoryID=t.CategoryID) " +
//			"on p.ProductID=D.ProductID) on m.OrderId=d.OrderId) " +
//			"left join customers c on c.CustomerID=m.CustomerID " +
//			"where m.OrderDate between '" + request.getParameter("BeginDate") + "' And '" + request.getParameter("EndDate") + "' " +
//			"order by t.CategoryName,p.ProductName,c.City,c.CompanyName ";
//		return Build(QuerySQL);
//	}

//	public static String FilterCrossPeriod(HttpServletRequest request) throws Exception {
//		String QuerySQL =  
//			"select d.ProductID,p.ProductName,m.OrderDate,d.UnitPrice*d.Quantity as Amount " +
//			"from (orders m inner join orderdetails as d on m.OrderId=d.OrderId) " +
//			"left join products p on d.ProductID=p.ProductID " +
//			"where m.OrderDate between '" + request.getParameter("BeginDate") + "' And '" + request.getParameter("EndDate") + "' " +
//			"order by p.ProductName, m.OrderDate";
//		return Build(QuerySQL);
//	}
//
//	public static String FilterChartBar(HttpServletRequest request) throws Exception {
//		String QuerySQL =  
//			"select p.CategoryID,t.CategoryName, c.City, sum(d.UnitPrice*d.Quantity) as Amount " +
//			"from (orders m inner join " +
//			"(orderdetails as d inner join (products p inner join categories t on p.CategoryID=t.CategoryID) " +
//			"on P.ProductID=D.ProductID) on m.OrderId=d.OrderId) " +
//			"left join customers c on c.CustomerID=m.CustomerID " +
//			"where m.OrderDate '" + request.getParameter("BeginDate") + "' And '" + request.getParameter("EndDate") + "' " +
//			"group by p.CategoryID,t.CategoryName, c.City " +
//			"order by c.City,t.CategoryName";
//		return Build(QuerySQL);
//	}
//
//	public static String FilterSaleDetail(HttpServletRequest request) throws Exception {
//		String KeyFilter = "";
//		String CategoryID = request.getParameter("CategoryID");
//		String ProductID = request.getParameter("ProductID");
//		String City = request.getParameter("City");
//		String CompanyName = request.getParameter("CompanyName");
//    
//		if (CategoryID != null)
//			KeyFilter += " and p.CategoryID=" + CategoryID;
//		if (ProductID != null)
//			KeyFilter += " and d.Productid=" + ProductID;
//		if (City != null)
//			KeyFilter += " and c.City='" + City + "'";
//		if (CompanyName != null)
//			KeyFilter += " and c.CompanyName='" + CompanyName + "'";
//
//		String QuerySQL = 
//			"select p.CategoryID,t.CategoryName,m.OrderId, m.OrderDate,c.City,c.CompanyName,d.Productid,p.ProductName,d.Quantity, " +
//			"d.UnitPrice * d.Quantity as Amount " +
//			"from (orders m inner join (orderdetails d inner join " +
//			"(products p inner join categories t on t.CategoryID = p.CategoryID) " +
//			"on d.ProductID = p.ProductID) on m.orderid = d.orderid) " +
//			"inner join customers c on m.CustomerID = c.CustomerID " +
//			"where m.OrderDate between '" + request.getParameter("BeginDate") + "' And '" + request.getParameter("EndDate") + "'" + KeyFilter + " " +
//			"order by p.CategoryID,m.OrderDate";
//		return Build(QuerySQL);
//	}

	public static String FreeGridwithDetailGrid() throws Exception {
		ArrayList<ReportQueryItem> QueryItems = new ArrayList<ReportQueryItem>();
		QueryItems.add(new ReportQueryItem("select * from employees where EmployeeID=8", "master"));
		QueryItems.add(new ReportQueryItem("select * from employees where EmployeeID<8", "detail"));
		return BuildList(QueryItems);
	}

	public static String InvoiceOne() throws Exception {
		ArrayList<ReportQueryItem> QueryItems = new ArrayList<ReportQueryItem>();
		QueryItems.add( new ReportQueryItem("select d.ProductID,p.ProductName,d.UnitPrice,d.Quantity,d.UnitPrice*d.Quantity as Amount "
			+ "from orderdetails as d inner join products p on P.ProductID=D.ProductID "
			+ "where d.OrderID=10255", "Master") );
		QueryItems.add( new ReportQueryItem("select m.OrderID,m.CustomerId,c.CompanyName,C.Address,m.OrderDate,c.ContactName+c.Phone as Remark "
			+ "from orders m left join customers c on c.CustomerID=m.CustomerID "
			+ "where m.OrderID=10255", "Detail") );
		return BuildList(QueryItems);
	}

	public static String SubReport_4a() throws Exception {
		ArrayList<ReportQueryItem> QueryItems = new ArrayList<ReportQueryItem>();
		QueryItems.add(new ReportQueryItem("select * from customers order by CustomerID", "Customer"));
		QueryItems.add(new ReportQueryItem("select * from products order by ProductName", "Product"));
		QueryItems.add(new ReportQueryItem(    "select c.CustomerID, c.CompanyName, sum(o.Quantity*o.UnitPrice) As SumAmt " +
			"from orderdetails o, orders m, customers c " +
			"where o.OrderID=m.OrderID and m.CustomerID=c.CustomerID " +
			"group by c.CustomerID, c.CompanyName " +
			"order by sum(o.Quantity*o.UnitPrice) desc", "Top10Customer"));
		QueryItems.add(new ReportQueryItem(    "select p.ProductID, p.ProductName, sum(o.Quantity*o.UnitPrice) As SumQty " +
			"from orderdetails o, products p " +
			"where o.ProductID=p.ProductID " +
			"group by p.ProductID, p.ProductName " +
			"order by sum(Quantity*o.UnitPrice) desc", "Top10Product"));
		return BuildList(QueryItems);
	}

	public static String SubReport_4b() throws Exception {
		ArrayList<ReportQueryItem> QueryItems = new ArrayList<ReportQueryItem>();
		QueryItems.add(new ReportQueryItem("select * from customers order by CustomerID", "Customer"));
		QueryItems.add(new ReportQueryItem("select * from products order by ProductName", "Product"));
		QueryItems.add(new ReportQueryItem("select * from customers order by CustomerID", "Customer2"));
		return BuildList(QueryItems);
	}

	public static String SubReport_4c() throws Exception {
		ArrayList<ReportQueryItem> QueryItems = new ArrayList<ReportQueryItem>();
		QueryItems.add( new ReportQueryItem("select * from orders where OrderID<=10260 order by OrderID", "Master") );
		QueryItems.add( new ReportQueryItem("select * from orderdetails where OrderID<=10260", "Detail1") );
		QueryItems.add( new ReportQueryItem("select o.OrderID, o.ShipCity, c.* from customers c, orders o " +
			"where OrderID<=10260 and c.City=o.ShipCity " +
			"order by o.OrderID", "Detail2") );
		return BuildList(QueryItems);
	}

//	public static String SubReport_4d(HttpServletRequest request) throws Exception {
//		//?这里传汉字参数没做对，如 北京，天津 到这里成为乱码
//		String RawCity = request.getParameter("city"); 
//		String City = new String(RawCity.getBytes("ISO-8859-1"), "UTF-8");  
//		//String City = request.getParameter("city"); 
//
//		return SubReport_4d(City);
//	}
	public static String SubReport_4d(String City) throws Exception {
		String CustomerQuerySQL = "select * from customers where City='" + City + "'";
		String SupplierQuerySQL = "select * from suppliers where City='" + City + "'";

		ArrayList<ReportQueryItem> QueryItems = new ArrayList<>();
		QueryItems.add(new ReportQueryItem(CustomerQuerySQL, "Customer"));
		QueryItems.add(new ReportQueryItem(SupplierQuerySQL, "Supplier"));
		return BuildList(QueryItems);
	}

	public static String SubReport_4e() throws Exception {
		ArrayList<ReportQueryItem> QueryItems = new ArrayList<ReportQueryItem>();
		QueryItems.add(new ReportQueryItem("select * from customers where CustomerID<'B' order by CustomerID", "Customer") );
		QueryItems.add(new ReportQueryItem("select * from orders where CustomerID<'B' " +
			"and OrderDate between '1/1/1997' and '12/31/1997' order by OrderID", "Order") );
		QueryItems.add(new ReportQueryItem("select D.* from orderdetails D, orders M " +
                "where M.CustomerID<'B' and M.OrderDate between #1/1/1997# and #12/31/1997# and D.OrderID =M.OrderID " +
                "order by D.OrderID", "OrderDetail") );
		return BuildList(QueryItems);
	}

	public static String Chart_8b() throws Exception {
		ArrayList<ReportQueryItem> QueryItems = new ArrayList<ReportQueryItem>();
		QueryItems.add(new ReportQueryItem("select * from scatter order by Name, X", "Table1"));
		QueryItems.add(new ReportQueryItem("select * from scatter order by Name, X", "Table2"));
		QueryItems.add(new ReportQueryItem("select * from scatter order by Name, X", "Table3"));
		QueryItems.add(new ReportQueryItem("select * from scatter order by Name, X", "Table4"));
		return BuildList(QueryItems);
	}

	public static String Chart_8d() throws Exception {
		ArrayList<ReportQueryItem> QueryItems = new ArrayList<ReportQueryItem>();
		String SQL = "select c.region,  d.ProductID,p.ProductName, " +
			"sum(d.UnitPrice * d.Quantity) as Amount " +
			"from(orders m inner join (orderdetails as d inner join products p on P.ProductID = D.ProductID) on m.OrderId = d.OrderId) " +
			"left join customers c on c.CustomerID = m.CustomerID " +
			"where d.ProductID in (1, 10, 11, 21) and m.OrderDate between '1997-1-1' and '1997-12-/31' " +
			"group by c.region, d.ProductID, p.ProductName " +
			"order by d.ProductID, c.region";
		String SQL2 = "select c.region, sum(d.UnitPrice * d.Quantity) as Amount, sum(d.Quantity) as Quantity " +
			"from(orders m inner join orderdetails d on m.OrderId = d.OrderId) " +
			"left join customers c on c.CustomerID = m.CustomerID " +
			"where d.ProductID = 11 and m.OrderDate between '1997-1-1' and '1997-12-/31' " +
			"group by c.region " +
			"order by c.region";

		QueryItems.add(new ReportQueryItem(SQL, "Table1"));
		QueryItems.add(new ReportQueryItem(SQL, "Table2"));
		QueryItems.add(new ReportQueryItem(SQL, "Table3"));
		QueryItems.add(new ReportQueryItem(SQL2, "Table4"));
		return BuildList(QueryItems);
	}

//	public static String BuildByHttpRequest(HttpServletRequest request) throws Exception
//	{
//		String text;
//
//		String dataID = request.getParameter("data"); 
//		if (dataID == null || dataID.length() == 0) {
//			//throw new Exception("没有提供 'data' 参数，无法获取到报表数据！");
//            String QuerySQL = request.getParameter("QuerySQL");
//			if (QuerySQL != null && QuerySQL.length() > 0) {
//                //根据传递的 HTTP 请求中的查询SQL获取数据
//                text = Build(QuerySQL);
//            }
//            else {
//				int DataLen = request.getContentLength();
//				if (DataLen > 0) {
//					//从客户端发送的数据包中获取报表查询参数，URL有长度限制，当要传递的参数数据量比较大时，应该采用这样的方式
//					//这里演示了用这样的方式传递一个超长查询SQL语句。
//					byte[] DataBuf = new byte[DataLen];   
//					ServletInputStream sif = request.getInputStream();
//					sif.read(DataBuf, 0, DataLen);
//					QuerySQL = new String(DataBuf);  
//					text = Build(QuerySQL);
//				}
//				else {
//					//throw new Exception("没有提供 'data' 参数，无法获取到报表数据！");
//					text = "";
//				}
//            }
//		}
//		else if (dataID.equals("AppendBlankRow")) {
//			text = AppendBlankRow();
//		}
//		else if (dataID.equals("Categories")) {
//			text = Categories();
//		}
//		else if (dataID.equals("ContractOne")) {
//			text = ContractOne();
//		}
//		else if (dataID.equals("CrossTab")) {
//			text = CrossTab();
//		}
//		else if (dataID.equals("CrossTabByDay")) {
//			text = CrossTabByDay();
//		}
//		else if (dataID.equals("CrossTabByMonth")) {
//			text = CrossTabByMonth();
//		}
//		else if (dataID.equals("CrossTabCalendar")) {
//			text = CrossTabCalendar();
//		}
//		else if (dataID.equals("CrossTabSubtotal")) {
//			text = CrossTabSubtotal();
//		}
//		else if (dataID.equals("CrossTabYearMonth")) {
//			text = CrossTabYearMonth();
//		}
//		else if (dataID.equals("Customer")) {
//			text = Customer();
//		}
//		else if (dataID.equals("EmployeeOne")) {
//			text = EmployeeOne();
//		}
//		else if (dataID.equals("InvoiceMany")) {
//			text = InvoiceMany();
//		}
//		else if (dataID.equals("Picture")) {
//			text = Picture();
//		}
//		else if (dataID.equals("RTFSample")) {
//			text = RTFSample();
//		}
//		else if (dataID.equals("SaleByProduct")) {
//			text = SaleByProduct();
//		}
//		else if (dataID.equals("SaleDetail")) {
//			text = SaleDetail();
//		}
//		else if (dataID.equals("SaleSumByProduct")) {
//			text = SaleSumByProduct();
//		}
//		else if (dataID.equals("Report_7_3g")) {
//			text = Report_7_3g(request);
//		}
//		else if (dataID.equals("FilterSaleSummary")) {
//			text = FilterSaleSummary(request);
//		}
//		else if (dataID.equals("FilterCustomerProductCrosstab")) {
//			text = FilterCustomerProductCrosstab(request);
//		}
//		else if (dataID.equals("FilterCrossPeriod")) {
//			text = FilterCrossPeriod(request);
//		}
//		else if (dataID.equals("FilterChartBar")) {
//			text = FilterChartBar(request);
//		}
//		else if (dataID.equals("FilterSaleDetail")) {
//			text = FilterSaleDetail(request);
//		}
//		else if (dataID.equals("FreeGridwithDetailGrid")) {
//			text = FreeGridwithDetailGrid();
//		}
//		else if (dataID.equals("InvoiceOne")) {
//			text = InvoiceOne();
//		}
//		else if (dataID.equals("SubReport_4a")) {
//			text = SubReport_4a();
//		}
//		else if (dataID.equals("SubReport_4b")) {
//			text = SubReport_4b();
//		}
//		else if (dataID.equals("SubReport_4c")) {
//			text = SubReport_4c();
//		}
//		else if (dataID.equals("SubReport_4d")) {
//			text = SubReport_4d(request);
//		}
//		else if (dataID.equals("SubReport_4e")) {
//			text = SubReport_4e();
//		}
//		else if (dataID.equals("Chart_8b")) {
//			text = Chart_8b();
//		}
//		else if (dataID.equals("Chart_8d")) {
//			text = Chart_8d();
//		}
//		else {
//			throw new Exception("没有为数据 '" + dataID + "' 分配处理程序，无法获取到报表数据！");
//		}
//
//		return text;
//	}
}
