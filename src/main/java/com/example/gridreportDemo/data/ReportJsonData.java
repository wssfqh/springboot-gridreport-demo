package com.example.gridreportDemo.data;

import java.sql.*;
import java.util.ArrayList;
import java.util.Base64;

//！注意：定义 oracle 数据库访问参数，应修改为与实际一致
class oracle_jdbc_param{   
	public final static String driver = "oracle.jdbc.driver.OracleDriver";   
	public final static String url = "jdbc:oracle:thin:@localhost:1521";   
	public final static String user = "hr";   
	public final static String password = "hr";   
}

//！注意：定义 mysql 数据库访问参数，应修改为与实际一致
class mysql_jdbc_param{   
	public final static String driver = "com.mysql.cj.jdbc.Driver"; //"com.mysql.jdbc.Driver"
	//public final static String url = "jdbc:mysql://localhost/gridreport?useUnicode=true&characterEncoding=utf8";
	public final static String url = "jdbc:mysql://localhost/gridreport?useUnicode=true&characterEncoding=utf8&serverTimezone=GMT%2B8&useSSL=false";
	public final static String user = "root";   
	public final static String password = "root";
}

//！注意：定义 mssql 数据库访问参数，应修改为与实际一致
class mssql_jdbc_param{   
//	public final static String driver = "com.microsoft.jdbc.sqlserver.SQLServerDriver";   //mssql2000 jdbc
//	public final static String url = "jdbc:microsoft:sqlserver://localhost;DatabaseName=gridreport"; //mssql2000 jdbc
	public final static String driver = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
	public final static String url = "jdbc:sqlserver://localhost;DatabaseName=SLMES";
	//如果是应用mssql20005的jdbc驱动，应该注视掉上面两行，而去掉下面两行的注视。说明：mssql2000 jdbc可以连接mssql20005 数据库
	//public final static String driver = "com.microsoft.sqlserver.jdbc.SQLServerDriver";    //mssql2005 jdbc
	//public final static String url = "jdbc:sqlserver://localhost;DatabaseName=gridreport"; //mssql2005 jdbc   
	public final static String user = "sa";   
	public final static String password = "5060526";


	//driver-class-name: net.sourceforge.jtds.jdbc.Driver
}

//！注意：定义 odbc 数据库访问参数，应修改为与实际一致
class odbc_jdbc_param{   
	public final static String driver = "sun.jdbc.odbc.JdbcOdbcDriver";   
	public final static String url = "jdbc:odbc:webreport";   
	public final static String user = "sa";   
	public final static String password = "";   
}

//！注意：如果是 oracle 数据库，请将 extends 后的类名改为 oracle_jdbc_param，并将 oracle_jdbc_param 中的参数改为与实际一致
//！注意：如果是 mysql 数据库，请将 extends 后的类名改为 mysql_jdbc_param，并将 mysql_jdbc_param 中的参数改为与实际一致
//！注意：如果是 mssql 数据库，请将 extends 后的类名改为 mssql_jdbc_param，并将 mssql_jdbc_param 中的参数改为与实际一致
//！注意：如果是 odbc 数据源，请将 extends 后的类名改为 odbc_jdbc_param，并将 odbc_jdbc_param 中的参数改为与实际一致
//public class jdbc_param extends oracle_jdbc_param 
//class jdbc_param extends mysql_jdbc_param
class jdbc_param extends mssql_jdbc_param
//public class jdbc_param extends odbc_jdbc_param 
{   
}

public class ReportJsonData {
	public static String MultiRecordset(ArrayList<ReportQueryItem> QueryItems) throws Exception
	{
	    Class.forName(jdbc_param.driver); // Class.forName 装载驱动程序
	    Connection con=DriverManager.getConnection(jdbc_param.url, jdbc_param.user, jdbc_param.password); //用适当的驱动程序类与 DBMS 建立一个连接
	    Statement stmt=con.createStatement(); //用于发送简单的SQL语句
	        
	    StringBuffer JsonText = new StringBuffer("{\n");
	    int Count = 1;
	    for (ReportQueryItem QueryItem : QueryItems)
	    {
	        DoGenOneRecordsetText(JsonText, QueryItem.QuerySQL, QueryItem.RecordsetName, stmt, (Count == QueryItems.size()));
	        ++Count;
	    }
	    JsonText.append('}');
	        
	    stmt.close();
	    con.close();
	        
		return JsonText.toString();
	}

	public static String OneRecordset(String QuerySQL) throws Exception
	{
	    Class.forName(jdbc_param.driver); // Class.forName 装载驱动程序
	    Connection con=DriverManager.getConnection(jdbc_param.url, jdbc_param.user, jdbc_param.password); //用适当的驱动程序类与 DBMS 建立一个连接
	    Statement stmt=con.createStatement(); //用于发送简单的SQL语句
	        
	    StringBuffer JsonText = new StringBuffer ("{\n");
	    DoGenOneRecordsetText(JsonText, QuerySQL, "row", stmt, true);
	    JsonText.append('}');
	        
	    stmt.close();
	    con.close();
	        
	    return JsonText.toString();
	}

	private static void DoGenOneRecordsetText(StringBuffer JsonText, String QuerySQL, String RecordsetName, Statement stmt, boolean LastRecordset)
	{
	    try
	    {
	        ResultSet rs=stmt.executeQuery(QuerySQL);

	        ResultSetMetaData rsmd = rs.getMetaData();
	        int ColCount = rsmd.getColumnCount();
	        
	        JsonText.append('"');
	        JsonText.append(RecordsetName);
	        JsonText.append("\":[\n");
	        boolean First = true;
	        while( rs.next() ) 
	        {
	            if (First)
	                First = false;
	            else
	                JsonText.append(",\n");
	            JsonText.append('{');
	            for (int i=1; i<=ColCount; i++)
	            {
	                JsonText.append('"');
	                JsonText.append(rsmd.getColumnLabel(i));
	                JsonText.append("\":\"");
	                
	                int ColType = rsmd.getColumnType(i);
	                if (ColType == Types.LONGVARBINARY || ColType == Types.VARBINARY || ColType == Types.BINARY || ColType == Types.BLOB)
	                {
	                    byte[] BinData = rs.getBytes(i);
	                    if ( !rs.wasNull() ) {
							//String base64Str = (new sun.misc.BASE64Encoder()).encode( BinData );
							//String base64Str = (new java.util.Base64()).encode( BinData );
	                    	//final Base64.Encoder encoder = Base64.getEncoder();
	                    	String base64Str = Base64.getEncoder().encodeToString(BinData);
							base64Str = base64Str.replaceAll("\n", "").replaceAll("\r", ""); //\r\n字符要去掉，不然HTML5报表解析json数据不成功
	                        JsonText.append(base64Str);
						}
	                }
	                else
	                {
	                    String Val = rs.getString(i);
	                    if ( !rs.wasNull() )
	                    {
	                        if ( JSON_HasSpecialChar(Val) )
	                            JsonText.append( JSON_Encode(Val) );
	                        else
	                            JsonText.append(Val);
	                    }
	                }
	                
	                JsonText.append('"');
	                if (i < ColCount)
	                    JsonText.append(',');
	            }
	            JsonText.append('}');
		    }
	        JsonText.append("\n]");
	        if ( !LastRecordset )
	            JsonText.append(',');
	        JsonText.append('\n');
	        
	        rs.close();
	    }
	    catch(Exception e)
	    {
	    	JsonText.append(e.toString());
	    }
	}

	//判断是否包含JSON特殊字符
	public static boolean JSON_HasSpecialChar(String text)
	{
	    if (text == null) 
	        return false;
	    
	    boolean ret = false;     
	    int len = text.length();
	    for (int i = 0; i < len; ++i)
	    {
	        char ch = text.charAt(i);
	        if (ch == '"' || ch == '\\' || ch == '\r' || ch == '\n' || ch == '\t')
	        {
	            ret = true;
	            break;
	        }
	    }
	    
	    return ret;
	}

	//判断是否包含JSON特殊字符
	public static String JSON_Encode(String text)
	{
	    int len = text.length();
	    StringBuffer results = new StringBuffer(len + 20);
	    
	    for (int i = 0; i < len; ++i)
	    {
	        char ch = text.charAt(i);
	        if (ch == '"' || ch == '\\' || ch == '\r' || ch == '\n' || ch == '\t')
	        {
	            results.append( '\\');
	            if (ch == '"' || ch == '\\')
	                results.append( ch  );
	            else if (ch == '\r')
	                results.append( 'r' );
	            else if (ch == '\n')
	                results.append( 'n' );
	            else if (ch == '\t')
	                results.append( 't' );
	        }
	        else
	        {
	            results.append( ch  );
	        }
	    }
	    
	    return results.toString();
	}
}