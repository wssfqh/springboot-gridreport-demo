package com.example.gridreportDemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GridreportDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(GridreportDemoApplication.class, args);
    }

}
