package com.example.gridreportDemo.util;

import gridreport.jni.ExportImageType;
import gridreport.jni.ExportType;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;
import java.net.URLEncoder;

public class ServerUtility {
    //如果是在 Windows 服务器，此参数不起作用，可以设定为任意值
    //如果是在 Linux 服务器，此参数必须与实际相符。指定目录下必须有 fonts 与 lang 这两个子目录
    //根据 grid++report 的程序模块实际部署目录设置 gridreport_module_path
    static public String gridreport_module_path = "/usr/local/grsvr6"; //用于默认发布

    // 将报表生成的二进制数据响应给 HTPP 请求客户端
    // <param name="context"> HTPP 请求对象</param>
    // <param name="ExportResult">报表生成的二进制数据</param>
    // <param name="FileName">指定下载(或保存)文件时的默认文件名称</param>
    // <param name="contentType">响应的ContentType</param>
    // <param name="OpenMode">指定生成的数据打开模式，可选[inline|attachment]，"inline"表示在网页中内联显示，"attachment"表示以附件文件形式下载。如果不指定，由浏览器自动确定打开方式。</param>
    static public void ResponseBinary(HttpServletRequest request, HttpServletResponse response,
                                      gridreport.jni.BinaryObject bo, String FileName, String contentType, String OpenMode) throws Exception {
        if (bo.getDataSize() > 0) {
            String Disposition = "";

            if (OpenMode != null && OpenMode.length() > 0) {
                Disposition = OpenMode + "; ";
            }

            String UserAgent = request.getHeader("User-Agent"); //?这样是否可行 context.Request.UserAgent
            Disposition += EncodeAttachmentFileName(UserAgent, FileName);

            response.setContentType(contentType);
            response.addHeader("Content-Length", new Integer(bo.getDataSize()).toString());
            response.addHeader("Content-Disposition", Disposition);

            response.resetBuffer();

            ServletOutputStream os = response.getOutputStream();
            os.write(bo.getDataBuf());
            os.flush();
        } else {
            ResponseException(response, "Failed to generate report.");
        }
    }

    // 将异常信息文字响应给请求的客户端
    // <param name="context"></param>
    // <param name="MessageText"></param>
    public static void ResponseException(HttpServletResponse response, String MessageText) throws Exception {
        PrintWriter pw = response.getWriter();
        pw.print(MessageText);
        pw.close();  //终止后续不必要内容输出
    }

    // 为了文件名中的汉字与特殊字符能正确，必须进行分浏览器处理
    // <param name="browserAgent"></param>
    // <param name="RawFileName"></param>
    // <returns></returns>
    public static String EncodeAttachmentFileName(String browserAgent, String RawFileName) throws Exception {
        String EncodedFileName = URLEncoder.encode(RawFileName, "utf-8");

        // 如果没有browserAgent，则默认使用IE的方式进行编码，因为毕竟IE还是占多数的
        String ret = "filename=\"" + EncodedFileName + "\"";
        if (browserAgent != null && browserAgent.length() > 0) {
            browserAgent = browserAgent.toLowerCase();
            // msie 与 edge 采用默认的方式
            if (!browserAgent.contains("msie") && !browserAgent.contains("edge")) {
                // Chrome浏览器，只能采用MimeUtility编码或ISO编码的中文输出
                if (browserAgent.contains("applewebkit")) {
                    //TODO...EncodedFileName = MimeUtility.encodeText(RawFileName, "UTF8", "B"); //？javax.mail.internet.MimeUtility这个不能用，造成chrome下载不成功
                    //ret = "filename*=UTF-8''" + EncodedFileName.replaceAll("\\+", "%20");
                    //ret = "filename=" + EncodedFileName.replaceAll("\\+", "%20");
                    //ret = "filename=\"" + RawFileName + "\"";
                }
                // Safari浏览器，只能采用ISO编码的中文输出
                else if (browserAgent.contains("safari")) {
                    //28591  iso-8859-1                1252   *
                    ret = "filename=\"" + new String(EncodedFileName.getBytes("UTF-8"), "ISO8859-1") + "\"";
                    //byte[] UTF8Bytes = UTF8Encoding.GetBytes(RawFileName);
                    //String ISO8859Text = System.Text.Encoding.GetEncoding(28591).GetString(UTF8Bytes);
                    //ret = "filename=\"" + ISO8859Text + "\"";
                }
                // Opera浏览器只能采用filename*
                // FireFox浏览器，可以使用MimeUtility或filename*或ISO编码的中文输出
                else if (browserAgent.contains("opera") || browserAgent.contains("mozilla")) {
                    ret = "filename*=UTF-8''" + EncodedFileName;
                }
            }
        }

        return ret;
    }

    public class ReportGenerateInfo {
        public String contentType;          //HTTP响应ContentType
        public String extFileName;          //默认扩展文件名
        public boolean isGRD;               //是否生成为 Grid++report 报表文档格式
        public ExportType exportType;     //导出的数据格式类型
        public ExportImageType imageType; //导出的图像格式类型

        ///根据报表导出格式类型，生成对应的响应信息，将结果信息保存本类的成员变量中
        ///参数 exportTypeText: 指定报表导出的导出格式类型
        ///参数 imageTypeText: 指定生成的图像格式，仅当为导出图像时有效
        public void Build(String exportTypeText, String imageTypeText) {
            extFileName = exportTypeText;
            contentType = "application/";
            isGRD = (exportTypeText == "grd" || exportTypeText == "grp");

            if (isGRD) {
                contentType += "octet-stream"; //application/octet-stream ?application/grd
            } else {
                switch (exportTypeText) {
                    case "xls":
                        exportType = ExportType.XLS;
                        contentType += "x-xls"; //application/vnd.ms-excel application/x-xls
                        break;
                    case "csv":
                        exportType = ExportType.CSV;
                        contentType += "vnd.ms-excel"; //application/vnd.ms-excel application/x-xls
                        break;
                    case "txt":
                        exportType = ExportType.TXT;
                        contentType = "text/plain"; //text/plain
                        break;
                    case "rtf":
                        exportType = ExportType.RTF;
                        contentType += "rtf"; //application/rtf
                        break;
                    case "img":
                        exportType = ExportType.IMG;
                        //contentType 要在后面根据图像格式来确定
                        break;
                    default:
                        extFileName = "pdf"; //"type"参数如没有设置，保证 extFileName 被设置为"pdf"
                        exportType = ExportType.PDF;
                        contentType += "pdf";
                        break;
                }

                //导出图像处理
                if (exportType == ExportType.IMG) {
                    extFileName = imageTypeText;
                    switch (imageTypeText) {
                        case "bmp":
                            imageType = ExportImageType.BMP;
                            contentType += "x-bmp";
                            break;
                        case "jpg":
                            imageType = ExportImageType.JPEG;
                            contentType += "x-jpg";
                            break;
                        case "tif":
                            imageType = ExportImageType.TIFF;
                            contentType = "image/tiff";
                            break;
                        default:
                            extFileName = "png";
                            imageType = ExportImageType.PNG;
                            contentType += "x-png";
                            break;
                    }
                }
            }
        }
    }
}
