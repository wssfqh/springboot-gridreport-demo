package com.example.gridreportDemo.controller;

import com.example.gridreportDemo.data.DataTextProvider;
import com.example.gridreportDemo.util.ServerUtility;
import gridreport.jni.BinaryObject;
import gridreport.jni.ExportType;
import gridreport.jni.Report;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ResourceLoader;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RestController
public class ReportController {

    @Autowired
    ResourceLoader resourceLoader;

    @GetMapping("test")
    public void test(HttpServletRequest request, HttpServletResponse response){
        Report report = null;
        try {
            report = new Report();
            String grfName = "1o.文字绕行与自动伸展.grf";
            String dataName = "jsonCategories.txt";
            String reportAbsolutePath = resourceLoader.getResource("classpath:report/"+grfName).getFile().getAbsolutePath();
            report.LoadFromFile(reportAbsolutePath);
            String dataAbsolutePath = resourceLoader.getResource("classpath:data/json/"+dataName).getFile().getAbsolutePath();
            report.LoadDataFromURL(dataAbsolutePath);
            BinaryObject bo = report.ExportDirectToBinaryObject(ExportType.HTM);
//            bo.SaveToFile("gr-jni-LoadDataFromURL.pdf");
            ServerUtility.ResponseBinary(request,response,bo,"test","text/html","inline");
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.getMessage());
        }
    }

    @GetMapping("test2")
    public void test2(HttpServletRequest request, HttpServletResponse response){
        Report report = null;
        try {
            report = new Report();
            String grfName = "2a.基本分组.grf";
            String dataName = "jsonCategories.txt";
            String reportAbsolutePath = resourceLoader.getResource("classpath:report/"+grfName).getFile().getAbsolutePath();
            report.LoadFromFile(reportAbsolutePath);
            String dataText = DataTextProvider.Customer();
            report.LoadDataFromXML(dataText);
            //设置打印相关的参数
            gridreport.jni.Printer printer = report.getPrinter();
            printer.setPrinterName("HP LaserJet Pro MFP M126nw");
            report.Print(true);
            System.out.println("Print " + reportAbsolutePath + "on printer name as " + printer.getPrinterName());
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.getMessage());
        }
    }
    @GetMapping("test3")
    public void test3(HttpServletRequest request, HttpServletResponse response){
        Report report = null;
        try {
            report = new Report();
            String grfName = "二维码测试.grf";
            String dataName = "jsonCategories.txt";
            String reportAbsolutePath = resourceLoader.getResource("classpath:report/"+grfName).getFile().getAbsolutePath();
            report.LoadFromFile(reportAbsolutePath);
            String dataText = DataTextProvider.QRCodeMESTM();
            report.LoadDataFromXML(dataText);
            //设置打印相关的参数
            gridreport.jni.Printer printer = report.getPrinter();
            printer.setPrinterName("HP LaserJet Pro MFP M126nw");
            report.Print(false);
            System.out.println("Print " + reportAbsolutePath + "on printer name as " + printer.getPrinterName());
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.getMessage());
        }
    }
}
